+++
date = "2014-01-01T12:00:00-04:00"
draft = false
title = "Disclosure"
alias = "http://www.makebeforebreak.com/disclosure/"
notablogpost = true
+++

**Make Before Break** is written and edited by [me][1].

I don't currently take any sponsorships, and any reviews posted are of items that I've bought and paid for with my own hard-earned money.

That said, I do occasionally post links to items on Amazon and on the iTunes and Mac App Stores. These links contain affiliate codes; if you click through and buy something, I get a small percentage. Someday, this may help to offset the costs of running this site.

While I do my best to be impartial and fair regarding any kind of reviews, you should take the above facts into account when reading this site.

[1]: https://angelostavrow.com