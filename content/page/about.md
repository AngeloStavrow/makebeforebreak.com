+++
date = "2014-01-01T12:00:00-04:00"
draft = false
title = "About"
alias = "http://www.makebeforebreak.com/about-mbb/"
notablogpost = true
+++

> In electrical engineering, make-before-break refers to a [type of switch][1], where the connection to the new position is made before breaking off contact with the original position. It's also known as a "shorting" switch, meaning that the process of flipping a switch temporarily connects the two terminals.

Make Before Break is a website about productivity, efficiency, quality, and occasional nerdery.

Making great stuff depends on far more than just what tools you use to get the job done. It's easy to think that a new calendar or to-do app will help improve your productivity, but there is no magic bullet. It's far more educational to explore why we procrastinate, why we "mail it in", and how we can use our weaknesses to our advantage.

That doesn't mean I don't have my favourite tools, nor that I won't share my workflows and opinions. But my goal is to stay lean, effective, and flexible—and the tools and workflows shared here are shared with that goal in mind.

I hope you enjoy reading.

[1]: http://en.wikipedia.org/wiki/Switch#Contact_terminology