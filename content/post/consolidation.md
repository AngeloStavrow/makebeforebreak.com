---
title: "Consolidation"
date: 2018-08-26T06:58:54-04:00
draft: false
---

Two weeks ago today, I relaunched [my personal site](https://angelostavrow.com). From here on out, that's where I'll be writing.

I've already moved all the posts from Make Before Break to that site; in a couple of weeks, I'll be redirecting this site there as well. If you're reading this in your favourite feed reader, you'll want to update that as well!

I'm sorry for the inconvenience; maintaining all these sites has been more administrative work than I'd like, and hopefully with less overhead, I can get back to doing more writing, and more _making_.